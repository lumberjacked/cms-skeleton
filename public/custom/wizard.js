$(document).ready(function() { 

    $.fn.wizard.logging = true;
      
      var wizard = $('#satellite-wizard').wizard({
        keyboard : false,
        contentHeight : 400,
        contentWidth : 700,
        backdrop: 'static',
        submitUrl: "{{ install_key }}"
        //'/api/$install_key/install.api/start'
      });

      wizard.on('submit', function(wizard) {

          $.ajax({
              type: 'POST',
              url: wizard.args.submitUrl,
              data: wizard.serialize(),
              dataType: 'json'
          }).done(function(responder) {
              
              console.log(responder);
              if(responder.response.error == true) {
                 wizard.submitError();
              } else {
                
                wizard.submitSuccess();         // displays the success card
                wizard.hideButtons();           // hides the next and back buttons
                wizard.updateProgressBar(0);    // sets the progress meter to 0
              }

          }).fail(function(response) {
              console.log(response);
              wizard.submitError();           // display the error card
              wizard.hideButtons();           // hides the next and back buttons
          });
      });
      
      wizard.on('closed', function() {
        wizard.reset();
      });

      wizard.on('reset', function() {
        wizard.modal.find(':input').val('').removeAttr('disabled');
        wizard.modal.find('.form-group').removeClass('has-error').removeClass('has-succes');
        wizard.modal.find('#fqdn').data('is-valid', 0).data('lookup', 0);
      });

      wizard.el.find('.wizard-success .im-done').click(function() {
        wizard.hide();
        setTimeout(function() {
          wizard.reset(); 
        }, 250);
        document.location.href='/admin';
        
      });
        
      wizard.el.find('.wizard-success').click(function() {
        wizard.reset();
      });
        
      $('#open-wizard').click(function(e) {
        e.preventDefault();
        wizard.show();
      });


});