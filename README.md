BraggAboutSystems CMS
=======================

Introduction
------------
This is a simple, content management system using ZF2 MVC layer and module
systems. This application is meant to be a simple replacement for Joomla, Wordpress, or Drupal.

Installation
------------

Clone or obtain copy of project
--------------------------------
    cd my/project/directory
    git clone https://bitbucket.org/lumberjacked/bas-cms.git
    cd bas-cms
    php composer.phar self-update
    php composer.phar install
    
(The `self-update` directive is to ensure you have an up-to-date `composer.phar`
available.)

### Apache Setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

    <VirtualHost *:80>
        ServerName cmsdomainname.com
        DocumentRoot /path/to/project/public
        SetEnv APPLICATION_ENV "production"
        <Directory /path/to/project/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
