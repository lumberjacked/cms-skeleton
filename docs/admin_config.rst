Admin Config
============

Displayed below are the core modules that make up the CMS.  They have been developed as seperate modules, but you need all of these to make the CMS function correctly.  This section is used to outline and display the actual classes for reading and browsing of the core code. 

.. toctree::
   :maxdepth: 2

   admin_controller_config
   admin_module_config
   admin_router_config
   admin_service_config
