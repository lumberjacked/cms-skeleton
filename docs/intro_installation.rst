Installation
============

.. contents:: Topics

.. _getting_bas_cms:

Getting Bas Cms
```````````````

You may also wish to follow the `Github project <https://github.com/lumberjacked/bas-cms>`_ if
you have a github account.  This is also where we keep the issue tracker for sharing
bugs and feature ideas.

.. _what_needs_to_be_installed:

Dependencies / What Needs To Be Installed
`````````````````````````````````````````

Installing Sphinx to run the docs ( Will move just quick dirty documentation now )

.. code-block:: bash    
    
    $ sudo apt-get install python-sphinx
    $ sudo apt-get install python-setuptools
    $ sudo easy_install pip
    $ sudo pip install sphinx_rtd_theme


Bas Cms by default has 4 database Adapters ready for use.  These will need to be installed on the server if you plan on using them.
  
  - MySql
  - Sqlite3

  .. code-block:: bash    
    
    $ sudo apt-get install sqlite3 libsqlite3-dev 
    $ sudo apt-get install php5-sqlite

  - MongoDB
  - CouchDB

.. _server_install:

Server Configuration
````````````````````

.. _from_git:

Cloning from Git
++++++++++++++++

To clone from version control

.. code-block:: bash    
    
    $ cd /var/www
    $ git clone https://bitbucket.org/lumberjacked/bas-cms.git
    $ cd ./bas-cms
    $ git tag -l
    $ git checkout tags/<latest_tag_name>





   

