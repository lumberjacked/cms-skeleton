BAS CMS Documentation
=====================

About BAS CMS
`````````````````````

Welcome to the CMS documentation!

BAS stands for BraggAboutSystems something I use for all my code as a catchy little acronym.
This documentation covers the current (under heavy development) version of Bas Cms (0.0.1).

.. _an_introduction:

.. toctree::
   :maxdepth: 2

   intro
   cmscore
   

