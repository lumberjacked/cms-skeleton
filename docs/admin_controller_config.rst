Controller Config
=================

:content: <a href="https://bitbucket.org/{{ bitbucket_user }}/{{ bitbucket_repo }}/src/{{ bitbucket_version}}{{ conf_py_path }}{{ pagename }}{{ source_suffix }}" class="fa fa-bitbucket"> Edit on Bitbucket</a>

.. literalinclude:: ../module/CmsAdmin/config/controller.config.php
   :language: php
   :linenos: