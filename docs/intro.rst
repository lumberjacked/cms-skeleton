Introduction
============

Before any other fun we need to install and configure a working copy of the CMS.  We will go over the installation, getting started in your new environment, and briefly discuss configuration options.  Something to remember is the CMS is built on top of ZF2, if your feeling clever and want to get your hands dirty just fork the project and add whatever you want.  Later we will discuss how to write a custom module (cms extension) that can be used in the CMS.  

.. toctree::
   :maxdepth: 1

   intro_installation
   intro_getting_started
   intro_configuration


